package com.example.memo.notifications

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.example.memo.R

class ReminderBroadcast : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
            val title = intent.getStringExtra("TITLE")
            val text = intent.getStringExtra("TEXT")
            val id = intent.getIntExtra("ID",200)

            var builder = NotificationCompat.Builder(context, "notifyUser")
                    .setSmallIcon(R.drawable.ic_notification)
                    .setContentTitle(title)
                    .setContentText(text)
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT)

            var notificationManager = NotificationManagerCompat.from(context)

            notificationManager.notify( id, builder.build())
    }
}