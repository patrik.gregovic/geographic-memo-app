package com.example.memo.data.repository

import android.content.Context
import androidx.room.Room
import com.example.memo.data.local.AppDatabase
import com.example.memo.data.model.Memo

class MemoRepository(applicationContext: Context) {

    private val db = Room.databaseBuilder(
        applicationContext,
        AppDatabase::class.java, "memo-db"
    ).build()

    private val memoDao = db.memoDao()

    suspend fun getAll(): List<Memo> {
        return memoDao.getAll()
    }

    suspend fun getLast(): Memo {
        return memoDao.getLast()[0]
    }

    suspend fun add(memo: Memo) {
        memoDao.insert(memo)
    }

    suspend fun remove(memo: Memo) {
        memoDao.delete(memo)
    }
}