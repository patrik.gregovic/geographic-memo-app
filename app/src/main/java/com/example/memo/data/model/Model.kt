package com.example.memo.data.model

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Entity
@Parcelize
data class Memo(
    @PrimaryKey(autoGenerate = true)
    val id: Int = 0,
    @SerializedName("memo_title")
    val title: String,
    @SerializedName("memo_time")
    val time: String,
    @SerializedName("memo_note")
    val note: String,
    @SerializedName("memo_location")
    val location: String,
): Parcelable









