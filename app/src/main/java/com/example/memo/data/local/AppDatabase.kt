package com.example.memo.data.local

import androidx.room.*
import com.example.memo.data.model.Memo

@Dao
interface MemoDao {
    @Query("SELECT * FROM memo")
    suspend fun getAll(): List<Memo>

    @Query("SELECT * FROM memo WHERE id = (SELECT MAX(id) FROM memo)")
    suspend fun getLast(): List<Memo>

    @Insert
    suspend fun insert(memo: Memo)

    @Delete
    suspend fun delete(memo: Memo)
}

@Database(entities = [Memo::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun memoDao(): MemoDao
}