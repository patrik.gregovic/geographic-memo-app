package com.example.memo.ui.forms

import android.app.AlarmManager
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Context.ALARM_SERVICE
import android.content.Context.NOTIFICATION_SERVICE
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.example.memo.R
import com.example.memo.data.model.Memo
import com.example.memo.data.repository.MemoRepository
import com.example.memo.databinding.NewMemoFragmentBinding
import com.example.memo.notifications.ReminderBroadcast
import com.google.android.material.datepicker.MaterialDatePicker
import com.google.android.material.timepicker.MaterialTimePicker
import com.google.android.material.timepicker.TimeFormat
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*


class NewMemoFragment : Fragment(R.layout.new_memo_fragment) {

    private lateinit var binding: NewMemoFragmentBinding

    private lateinit var memoRepository: MemoRepository


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = NewMemoFragmentBinding.bind(view)

        memoRepository = MemoRepository(requireContext())

        binding.backBtn.setOnClickListener {
            //set direction
            findNavController().popBackStack()
        }

        var dateTimeString = ""
        binding.etDateInput.setOnClickListener {

            val datePicker =
                MaterialDatePicker.Builder.datePicker()
                    .setTitleText("Select date")
                    .build()

            val timePicker =
                MaterialTimePicker.Builder()
                    .setTimeFormat(TimeFormat.CLOCK_24H)
                    .setTitleText("Select Appointment time")
                    .build()

            timePicker.addOnPositiveButtonClickListener{
                val hour = timePicker.hour
                val minute = timePicker.minute

                dateTimeString += " ${hour}:${minute}"

                binding.etDateInput.setText(dateTimeString)
            }

            datePicker.addOnPositiveButtonClickListener {
                val date = datePicker.selection
                dateTimeString = SimpleDateFormat("d/M/yyy").format(date)


                activity?.let { it_activity ->
                    timePicker.show(it_activity.supportFragmentManager, "tag")
                };
            }

            activity?.let { it_activity ->
                datePicker.show(it_activity.supportFragmentManager, "tag")
            };

        }

        binding.submitBtn.setOnClickListener {

            val df: DateFormat = SimpleDateFormat("d/M/yyy HH:mm")
            val cal: Calendar = Calendar.getInstance()
            cal.time = df.parse(dateTimeString)

            lifecycleScope.launch(Dispatchers.IO) {
                memoRepository.add(
                    Memo(
                        title = binding.etTitleInput.text.toString(),
                        time = binding.etDateInput.text.toString(),
                        note = binding.etNoteInput.text.toString(),
                        location = binding.etLocationInput.text.toString(),
                    )
                )

                val newMemo = memoRepository.getLast()
                setAlarm(newMemo, cal)
            }



            //set direction
            findNavController().popBackStack()
        }


    }

    fun setAlarm(memo: Memo, cal: Calendar){

        val intent = Intent(context, ReminderBroadcast::class.java)
        intent.putExtra("TITLE", "Memo - ${binding.etTitleInput.text}")
        intent.putExtra("TEXT", "${binding.etNoteInput.text} at ${binding.etLocationInput.text}")
        intent.putExtra("ID", memo.id)
        println("MEMO ID IS ${memo.id}")
        val pendingIntent: PendingIntent = PendingIntent.getBroadcast(context, memo.id, intent, 0)

        val alarmManager: AlarmManager = requireContext().getSystemService(ALARM_SERVICE) as AlarmManager

        val reminderTime: Long = cal.timeInMillis

        alarmManager.set(AlarmManager.RTC_WAKEUP, reminderTime, pendingIntent)
    }

}