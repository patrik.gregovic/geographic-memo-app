package com.example.memo.ui

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.memo.R
import com.example.memo.data.model.Memo
import com.example.memo.databinding.MemoFragmentBinding


class MemoAdapter(private val onTap: (memo: Memo) -> Unit):
        RecyclerView.Adapter<MemoAdapter.MemoItem>() {

    private var data: List<Memo> = listOf()

    class MemoItem(itemView: View): RecyclerView.ViewHolder(itemView) {
        private val binding = MemoFragmentBinding.bind(itemView)

        fun configure(memo: Memo) {
            println(memo.id)
            println(memo.title)
            println(memo.time)
            println(memo.note)
            println(memo.location)
            //binding.tvId.text = memo.id.toString()
            binding.tvTitle.text = memo.title
            binding.tvDate.text = memo.time
            //binding.tvNote.text = memo.note
            //binding.tvLocation.text = memo.location
        }
    }

    fun updateData(memoData: List<Memo>) {
        data = memoData
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int = data.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MemoItem {
        val layoutInflater = LayoutInflater.from(parent.context)
        val itemView = layoutInflater.inflate(R.layout.memo_fragment, parent, false)
        return MemoItem(itemView)
    }

    override fun onBindViewHolder(holder: MemoItem, position: Int) {
        val memo = data[position]
        holder.configure(memo)
        holder.itemView.setOnClickListener {
            onTap(memo)
        }
    }
}

