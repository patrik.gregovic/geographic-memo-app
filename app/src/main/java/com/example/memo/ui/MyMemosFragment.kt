package com.example.memo.ui

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.example.memo.R
import com.example.memo.data.model.Memo
import com.example.memo.data.repository.MemoRepository
import com.example.memo.databinding.MymemosFragmentBinding
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.text.SimpleDateFormat

class MyMemosFragment : Fragment(R.layout.mymemos_fragment){

    private lateinit var binding: MymemosFragmentBinding

    private lateinit var memoRepository: MemoRepository



    private val memoAdapter = MemoAdapter {
        navigateToDetail(it)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = MymemosFragmentBinding.bind(view)

        memoRepository = MemoRepository(requireContext())

        binding.memosList.adapter = memoAdapter

        lifecycleScope.launch(Dispatchers.IO) {
            val data = memoRepository.getAll()
            withContext(Dispatchers.Main) {
                memoAdapter.updateData(data)
            }

        }

        binding.submitBtn.setOnClickListener {
            //set direction
            val direction =  MyMemosFragmentDirections.actionMyMemosFragmentToNewMemoFragment()
            findNavController().navigate(direction)
        }
    }

    private fun navigateToDetail(memo: Memo) {
        val direction = MyMemosFragmentDirections.actionMyMemosFragmentToDetailFragment(memo)
        findNavController().navigate(direction)
    }

}