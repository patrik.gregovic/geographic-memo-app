package com.example.memo.ui.detail

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import android.widget.ImageView
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.example.memo.R
import com.example.memo.data.model.Memo
import com.example.memo.data.repository.MemoRepository
import com.example.memo.databinding.DetailFragmentBinding
import com.example.memo.notifications.ReminderBroadcast
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.net.URLEncoder

class DetailFragment : Fragment(R.layout.detail_fragment) {
    private lateinit var binding: DetailFragmentBinding
    private val args: DetailFragmentArgs by navArgs()

    private lateinit var memoRepository: MemoRepository

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = DetailFragmentBinding.bind(view)
        memoRepository = MemoRepository(requireContext())

        binding.backBtn.setOnClickListener {
            //set direction
            findNavController().popBackStack()
        }


        binding.deleteBtn.setOnClickListener {

            val intent = Intent(context, ReminderBroadcast::class.java)
            val pendingIntent: PendingIntent = PendingIntent.getBroadcast(context, args.memo.id, intent, 0)
            val alarmManager: AlarmManager = requireContext().getSystemService(Context.ALARM_SERVICE) as AlarmManager
            alarmManager.cancel(pendingIntent)
            pendingIntent.cancel()

            lifecycleScope.launch(Dispatchers.IO) {
                memoRepository.remove(args.memo)
            }
            //set direction
            findNavController().popBackStack()
        }


        binding.tvTitle.text = args.memo.title
        binding.tvDate.text = args.memo.time
        binding.tvNote.text = args.memo.note
        binding.tvLocation.text = args.memo.location

        val location = args.memo.location
        val locationEncoded = URLEncoder.encode(location, "utf-8")

        val staticMap = binding.ivLocation
        staticMap.viewTreeObserver.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                val width = staticMap.maxWidth
                val height = staticMap.maxHeight


                val mapUrl = "https://maps.googleapis.com/maps/api/staticmap" +
                        "?center=${locationEncoded}" + //define the center point
                        "&zoom=16&size=${300}x${300}&scale=2" + //define the zoom and size
                        "&key=AIzaSyDB_SPnyxx6rtTO_WyDi6b7kPVQ072ob3E" +
                        "&markers=color:0xB600D3%7Csize:large%7C${locationEncoded}" + //the rest is just styling of the map
                        "&sensor=false&format=png&style=feature:poi|element:labels|visibility:off"+
                        "&style=element:geometry|color:0xf5f5f5&style=element:labels.icon|visibility:off&style=element:labels.text.fill" +
                        "|color:0x616161&style=element:labels.text.stroke|color:0xf5f5f5&style=feature:administrative.land_parcel" +
                        "|element:labels.text.fill|color:0xbdbdbd&style=feature:landscape|color:0xededed&style=feature:poi" +
                        "|element:geometry|color:0xeeeeee&style=feature:poi|element:labels.text.fill|color:0x757575&style=feature:poi.park" +
                        "|element:geometry|color:0xe5e5e5&style=feature:poi.park|element:labels.text.fill|color:0x9e9e9e&style=feature:road" +
                        "|element:geometry|color:0xffffff&style=feature:road.arterial|element:labels.text.fill|color:0x757575&style=feature:road.highway" +
                        "|element:geometry|color:0xdadada&style=feature:road.highway|element:labels.text.fill|color:0x616161&style=feature:road.local" +
                        "|element:labels.text.fill|color:0x9e9e9e&style=feature:transit.line|element:geometry|color:0xe5e5e5&style=feature:transit.station" +
                        "|element:geometry|color:0xeeeeee&style=feature:water|element:geometry|color:0xc9c9c9&style=feature:water|element:geometry.fill" +
                        "|color:0xbababa|visibility:on&style=feature:water|element:labels.text.fill|color:0x9e9e9e"

                println(mapUrl)

                staticMap.loadUrl(mapUrl)
            }
        })


    }



    fun ImageView.loadUrl(url: String?) {
        url?.let {
            Glide.with(this).load(it).into(this)
        }
    }

}