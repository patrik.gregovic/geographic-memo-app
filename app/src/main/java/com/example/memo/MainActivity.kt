package com.example.memo

import android.app.NotificationChannel
import android.app.NotificationManager
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupActionBarWithNavController

class MainActivity : AppCompatActivity() {
    private lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        createNotificationChannel()

        val navHostFragment = supportFragmentManager.findFragmentById(R.id.navHost) as NavHostFragment
        navController = navHostFragment.navController
    }

    override fun onSupportNavigateUp(): Boolean {
        return navController.popBackStack()
    }

    private fun createNotificationChannel(){

        //if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            val name: CharSequence = "UserNotificationChannel"

            val description: String = "Channel for notifying users"

            val importance: Int = NotificationManager.IMPORTANCE_DEFAULT

            val channel: NotificationChannel = NotificationChannel("notifyUser", name, importance)
            channel.description = description

            val notificationManager = getSystemService(NotificationManager::class.java)

            notificationManager.createNotificationChannel(channel)

       // }

    }

}